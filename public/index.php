<?php    
require 'tropo.class.php';

$tropo = new Tropo();    
// Use Tropo's text to speech to say a phrase.    
$tropo->say('Yes, Tropo is this easy.');    

// Render the JSON back to Tropo.
$tropo->renderJSON();    
?>  